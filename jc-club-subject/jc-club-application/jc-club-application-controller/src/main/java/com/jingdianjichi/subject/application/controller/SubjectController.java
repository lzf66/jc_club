package com.jingdianjichi.subject.application.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Preconditions;
import com.jingdianjichi.subject.application.convert.SubjectAnswerDTOConvert;
import com.jingdianjichi.subject.application.convert.SubjectInfoDTOConvert;
import com.jingdianjichi.subject.application.dto.SubjectInfoDTO;
import com.jingdianjichi.subject.common.entity.PageResult;
import com.jingdianjichi.subject.common.entity.Result;
import com.jingdianjichi.subject.domain.entity.SubjectAnswerBO;
import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import com.jingdianjichi.subject.domain.service.SubjectInfoDomainService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/*刷题controller*/
@RestController
@Slf4j
@RequestMapping("/subject")
public class SubjectController {


   @Resource
   private SubjectInfoDomainService subjectInfoDomainService;



/*新增题目*/
@PostMapping("/add")
public Result<Boolean> add(@RequestBody SubjectInfoDTO subjectInfoDTO){

    try {
        if(log.isInfoEnabled()){
            log.info("SubjectController.add.dto:{}", JSON.toJSONString(subjectInfoDTO));
        }
        Preconditions.checkNotNull(subjectInfoDTO.getSubjectDifficult(),"题目难度不能为空");
        Preconditions.checkNotNull(subjectInfoDTO.getSubjectType(),"题目分类不能为空");
        Preconditions.checkNotNull(subjectInfoDTO.getSubjectName(),"题目名称不能为空");
        Preconditions.checkNotNull(subjectInfoDTO.getSubjectScore(),"题目得分不能为空");
        Preconditions.checkArgument(!CollectionUtils.isEmpty(subjectInfoDTO.getLabelIds()),"标签ID不能为空");
        Preconditions.checkArgument(!CollectionUtils.isEmpty(subjectInfoDTO.getCategoryIds()),"分类ID不能为空");


        SubjectInfoBO subjectInfoBO = SubjectInfoDTOConvert.INSTANCE.convertDTOToBO(subjectInfoDTO);
        List<SubjectAnswerBO> subjectAnswerBOS =
                SubjectAnswerDTOConvert.INSTANCE.convertDtoToSubjectAnswerBoLT(subjectInfoDTO.getOptionList());
        subjectInfoBO.setOptionList(subjectAnswerBOS);
        subjectInfoDomainService.add(subjectInfoBO);
        return Result.ok(true);
    }catch (Exception e){
        log.error("SubjectInfoController.add.error:{}",e.getMessage(),e);
        return Result.fail(e.getMessage());
    }
}

/*查询题目列表*/
    @PostMapping("/getSubjectPage")
    public Result<PageResult<SubjectInfoDTO>> getSubjectPage(@RequestBody SubjectInfoDTO subjectInfoDTO){

        try {
            if(log.isInfoEnabled()){
                log.info("SubjectController.queryPage.dto:{}", JSON.toJSONString(subjectInfoDTO));
            }
            Preconditions.checkNotNull(subjectInfoDTO.getCategoryId(), "分类id不能为空");
            Preconditions.checkNotNull(subjectInfoDTO.getLabelId(), "标签id不能为空");
            SubjectInfoBO subjectInfoBO = SubjectInfoDTOConvert.INSTANCE.convertDTOToBO(subjectInfoDTO);
            subjectInfoBO.setPageNo(subjectInfoDTO.getPageNo());
            subjectInfoBO.setPageSize(subjectInfoDTO.getPageSize());

            PageResult<SubjectInfoBO> pageResult = subjectInfoDomainService.getSubjectPage(subjectInfoBO);
            return Result.ok(pageResult);
        }catch (Exception e){
            log.error("SubjectInfoController.queryPage.error:{}",e.getMessage(),e);
            return Result.fail(e.getMessage());
        }
    }

    /*查询题目信息*/
    @PostMapping("/querySubjectInfo")
    public Result<PageResult<SubjectInfoDTO>> querySubjectInfo(@RequestBody SubjectInfoDTO subjectInfoDTO){

        try {
            if(log.isInfoEnabled()){
                log.info("SubjectInfoController.querySubjectInfo.dto:{}", JSON.toJSONString(subjectInfoDTO));
            }
//            Preconditions.checkNotNull(subjectInfoDTO.getCategoryId(), "分类id不能为空");
//            Preconditions.checkNotNull(subjectInfoDTO.getLabelId(), "标签id不能为空");
            SubjectInfoBO subjectInfoBO = SubjectInfoDTOConvert.INSTANCE.convertDTOToBO(subjectInfoDTO);
            SubjectInfoBO bo = subjectInfoDomainService.querySubjectInfo(subjectInfoBO);
            SubjectInfoDTO dto = SubjectInfoDTOConvert.INSTANCE.convertBOToDTO(bo);
            return Result.ok(dto);
        }catch (Exception e){
            log.error("SubjectInfoController.querySubjectInfo.error:{}",e.getMessage(),e);
            return Result.fail(e.getMessage());
        }
    }
}

