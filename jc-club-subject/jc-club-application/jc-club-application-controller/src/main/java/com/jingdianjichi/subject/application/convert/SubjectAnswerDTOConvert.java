package com.jingdianjichi.subject.application.convert;

import com.jingdianjichi.subject.application.dto.SubjectAnswerDTO;
import com.jingdianjichi.subject.domain.entity.SubjectAnswerBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SubjectAnswerDTOConvert {
SubjectAnswerDTOConvert INSTANCE = Mappers.getMapper(SubjectAnswerDTOConvert.class);

SubjectAnswerBO convertDtoToSubjectAnswerBO(SubjectAnswerDTO subjectAnswerDTO);

    List<SubjectAnswerBO> convertDtoToSubjectAnswerBoLT(List<SubjectAnswerDTO> dtoList);





}
