package com.jingdianjichi.subject.application.dto;

import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 题目标签表(SubjectLabel)实体类
 *
 * @author makejava
 * @since 2024-06-20 15:14:01
 */
@Data
public class SubjectLabelDTO implements Serializable {
    private static final long serialVersionUID = 927303677838132337L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 标签分类
     */
    private String labelName;
    /**
     * 排序
     */
    private Integer sortNum;

    /*分类ID*/
    private Long categoryId;
    

}

