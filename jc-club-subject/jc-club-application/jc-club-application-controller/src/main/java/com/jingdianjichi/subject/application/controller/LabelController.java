package com.jingdianjichi.subject.application.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Preconditions;
import com.jingdianjichi.subject.application.convert.SubjectCategoryDTOConvert;
import com.jingdianjichi.subject.application.convert.SubjectLabelDTOConvert;
import com.jingdianjichi.subject.application.dto.SubjectCategoryDTO;
import com.jingdianjichi.subject.application.dto.SubjectLabelDTO;
import com.jingdianjichi.subject.common.entity.Result;
import com.jingdianjichi.subject.domain.convert.SubjectLabelConvert;
import com.jingdianjichi.subject.domain.entity.SubjectCategoryBO;
import com.jingdianjichi.subject.domain.entity.SubjectLabelBO;
import com.jingdianjichi.subject.domain.service.SubjectLabelDomainService;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("/subject/label")
public class LabelController {
    @Resource
    private SubjectLabelDomainService subjectLabelDomainService;
    /*新增标签*/
    @PostMapping("/add")
    public Result<Boolean> add(@RequestBody SubjectLabelDTO subjectLabelDTO){

        try {
            if(log.isInfoEnabled()){
                log.info("SubjectLabelController.add.dto:{}", JSON.toJSONString(subjectLabelDTO));
            }
            Preconditions.checkArgument(!StringUtils.isBlank(subjectLabelDTO.getLabelName()),"分类名称不能为空");
            SubjectLabelBO subjectLabelBO = SubjectLabelDTOConvert.INSTANCE.convertDToToLabelBO(subjectLabelDTO);
            Boolean result = subjectLabelDomainService.add(subjectLabelBO);
            return Result.ok(result);
        }catch (Exception e){
            log.error("SubjectLabelController.add.erro:{}",e.getMessage(),e);
            return Result.fail(e.getMessage());
        }
    }
    /*更新标签*/
    @PostMapping("/update")
    public Result<Boolean> update(@RequestBody SubjectLabelDTO subjectLabelDTO){
        try {

            SubjectLabelBO subjectLabelBO = SubjectLabelDTOConvert.INSTANCE.convertDToToLabelBO(subjectLabelDTO);
            Boolean result = subjectLabelDomainService.update(subjectLabelBO);

            return Result.ok(result);
        }catch (Exception e){
            log.error("SubjectLabelController.update.erro:{}",e.getMessage(),e);
            return Result.fail("更新失败");
        }
    }

    /*删除标签*/
    @PostMapping("/delete")
    public Result<Boolean> delete(@RequestBody SubjectLabelDTO subjectLabelDTO){
        try {
            SubjectLabelBO subjectLabelBO = SubjectLabelDTOConvert.INSTANCE.convertDToToLabelBO(subjectLabelDTO);
            Boolean result = subjectLabelDomainService.delete(subjectLabelBO);
            return Result.ok(result);
        }catch (Exception e){
            log.error("SubjectLabelController.update.erro:{}",e.getMessage(),e);
            return Result.fail("更新失败");
        }
    }
    /*根据分类查询标签*/
    @PostMapping("/queryLabelByCategoryId")
    public Result<List<SubjectLabelDTO>> queryLabelByCategoryId(@RequestBody SubjectLabelDTO subjectLabelDTO){
        try {
            SubjectLabelBO subjectLabelBO = SubjectLabelDTOConvert.INSTANCE.convertDToToLabelBO(subjectLabelDTO);
            List<SubjectLabelBO> result = subjectLabelDomainService.queryLabelByCategoryId(subjectLabelBO);
            List<SubjectLabelDTO> dtoList = SubjectLabelDTOConvert.INSTANCE.convertBoToLabelList(result);
            return Result.ok(dtoList);
        }catch (Exception e){
            log.error("SubjectLabelController.update.erro:{}",e.getMessage(),e);
            return Result.fail("更新失败");
        }
    }

}

