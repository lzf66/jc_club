package com.jingdianjichi.subject.application.convert;

import com.jingdianjichi.subject.application.dto.SubjectCategoryDTO;
import com.jingdianjichi.subject.domain.entity.SubjectCategoryBO;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SubjectCategoryDTOConvert {
SubjectCategoryDTOConvert INSTANCE = Mappers.getMapper(SubjectCategoryDTOConvert.class);
SubjectCategoryBO convertDToToCategoryBO(SubjectCategoryDTO subjectCategoryDTO);

List<SubjectCategoryDTO> convertCategoryLToDTOL(List<SubjectCategoryBO> subjectCategoryBOList);

    //SubjectCategoryBO convertDTOCategoryBO(SubjectCategoryDTO );
}
