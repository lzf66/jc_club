package com.jingdianjichi.subject.application.controller;

import com.alibaba.fastjson.JSON;
import com.google.common.base.Preconditions;
import com.jingdianjichi.subject.application.convert.SubjectCategoryDTOConvert;
import com.jingdianjichi.subject.application.dto.SubjectCategoryDTO;
import com.jingdianjichi.subject.common.entity.Result;

import com.jingdianjichi.subject.domain.entity.SubjectCategoryBO;
import com.jingdianjichi.subject.domain.service.SubjectCategoryDomainService;


import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;

/*刷题controller*/
@Slf4j
@RestController
@RequestMapping("/subject/category")
public class SubjectCategoryController {
   @Resource
   private SubjectCategoryDomainService subjectCategoryDomainService;

   /*新增种类*/
    @PostMapping("/add")
    public Result<Boolean> add(@RequestBody SubjectCategoryDTO subjectCategoryDTO){

        try {
            if(log.isInfoEnabled()){
                log.info("SubjectCategoryController.add.dto:{}", JSON.toJSONString(subjectCategoryDTO));
            }
            //Preconditions.checkNotNull(subjectCategoryDTO.getCategoryName(),"菜品名字不能为空");
            Preconditions.checkNotNull(subjectCategoryDTO.getCategoryType(),"分类类型不能为空");
            Preconditions.checkArgument(!StringUtils.isBlank(subjectCategoryDTO.getCategoryName()),"分类名称不能为空");
            Preconditions.checkNotNull(subjectCategoryDTO.getParentId(),"分类parentID不能为空");
            SubjectCategoryBO subjectCategoryBO = SubjectCategoryDTOConvert.INSTANCE.convertDToToCategoryBO(subjectCategoryDTO);
            subjectCategoryDomainService.add(subjectCategoryBO);
            return Result.ok(true);
        }catch (Exception e){
            log.error("SubjectCategoryController.add.erro:{}",e.getMessage(),e);
            return Result.fail(e.getMessage());
        }
    }
    /*查询分类*/
    @PostMapping("/queryPrimaryCategory")
    public Result<List<SubjectCategoryDTO>> queryPrimaryCategory(@RequestBody SubjectCategoryDTO subjectCategoryDTO){
        try {
            SubjectCategoryBO subjectCategoryBo = SubjectCategoryDTOConvert.INSTANCE.convertDToToCategoryBO(subjectCategoryDTO);
            List<SubjectCategoryBO> subjectCategoryBOList = subjectCategoryDomainService.queryCategory(subjectCategoryBo);
            List<SubjectCategoryDTO> subjectCategoryDTOList = SubjectCategoryDTOConvert.INSTANCE.convertCategoryLToDTOL(subjectCategoryBOList);
            System.out.println(subjectCategoryDTOList);
            return Result.ok(subjectCategoryDTOList);
        }catch (Exception e){
            log.error("SubjectCategoryController.queryPrimaryCategory.erro:{}",e.getMessage(),e);
            return Result.fail("查询失败");
        }
    }

    /*通过岗位查询分类*/
    @PostMapping("/queryCategoryByPrimary")
    public Result<List<SubjectCategoryDTO>> queryCategoryByPrimary(@RequestBody SubjectCategoryDTO subjectCategoryDTO){
        try {
            if(log.isInfoEnabled()){
                log.info("SubjectCategoryController.queryCategoryByPrimary.dto:{}", JSON.toJSONString(subjectCategoryDTO));
            }
            Preconditions.checkNotNull(subjectCategoryDTO.getParentId(),"id不能为空");
            SubjectCategoryBO subjectCategoryBo = SubjectCategoryDTOConvert.INSTANCE.convertDToToCategoryBO(subjectCategoryDTO);
            List<SubjectCategoryBO> subjectCategoryBOList = subjectCategoryDomainService.queryCategory(subjectCategoryBo);
            List<SubjectCategoryDTO> subjectCategoryDTOList = SubjectCategoryDTOConvert.INSTANCE.convertCategoryLToDTOL(subjectCategoryBOList);
            System.out.println(subjectCategoryDTOList);
            return Result.ok(subjectCategoryDTOList);
        }catch (Exception e){
            log.error("SubjectCategoryController.queryPrimaryCategory.erro:{}",e.getMessage(),e);
            return Result.fail("查询失败");
        }
    }

    /*更新分类*/
    @PostMapping("/update")
    public Result<Boolean> update(@RequestBody SubjectCategoryDTO subjectCategoryDTO){
        try {
           // SubjectCategoryBO subjectCategoryBO = new SubjectCategoryBO();
            SubjectCategoryBO subjectCategoryBo = SubjectCategoryDTOConvert.INSTANCE.convertDToToCategoryBO(subjectCategoryDTO);
            Boolean result = subjectCategoryDomainService.update(subjectCategoryBo);


            return Result.ok(result);
        }catch (Exception e){
            log.error("SubjectCategoryController.update.erro:{}",e.getMessage(),e);
            return Result.fail("更新失败");
        }
    }

    @PostMapping("/delete")
    public Result<Boolean> delete(@RequestBody SubjectCategoryDTO subjectCategoryDTO){
        try {

            SubjectCategoryBO subjectCategoryBo = SubjectCategoryDTOConvert.INSTANCE.convertDToToCategoryBO(subjectCategoryDTO);
            Boolean result = subjectCategoryDomainService.delete(subjectCategoryBo);
            return Result.ok(result);
        }catch (Exception e){
            log.error("SubjectCategoryController.update.erro:{}",e.getMessage(),e);
            return Result.fail("更新失败");
        }
    }
}

