package com.jingdianjichi.subject.domain.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 题目标签表BO
 *
 * @author makejava
 * @since 2024-06-20 15:14:01
 */
@Data
public class SubjectLabelBO implements Serializable {
    private static final long serialVersionUID = 927303677838132337L;
    /**
     * 主键
     */
    private Long id;
    /**
     * 标签分类
     */
    private String labelName;
    /**
     * 排序
     */
    private Integer sortNum;

    private Long categoryId;
    

}

