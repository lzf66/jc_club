package com.jingdianjichi.subject.domain.entity;

import lombok.Data;

import java.io.Serializable;

/**
 * 题目答案DTO
 *
 * @author makejava
 * @since 2024-06-20 21:21:50
 */
@Data
public class SubjectAnswerBO implements Serializable {
    private static final long serialVersionUID = 477488215523701550L;
    /**
     * 题目选项标识
     */
    private Integer optionType;
    /**
     * 答案
     */
    private String optionContent;
    /**
     * 是否正确
     */
    private Integer isCorrect;

}

