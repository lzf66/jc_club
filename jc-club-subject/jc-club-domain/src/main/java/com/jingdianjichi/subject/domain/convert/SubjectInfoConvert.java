package com.jingdianjichi.subject.domain.convert;

import com.jingdianjichi.subject.domain.entity.SubjectAnswerBO;
import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import com.jingdianjichi.subject.domain.entity.SubjectOptionBO;
import com.jingdianjichi.subject.infra.basic.entity.SubjectInfo;
import com.jingdianjichi.subject.infra.basic.entity.SubjectJudge;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SubjectInfoConvert {
SubjectInfoConvert INSTANCE = Mappers.getMapper(SubjectInfoConvert.class);

    SubjectInfo convertBoToInfo(SubjectInfoBO subjectInfoBO);
    List<SubjectInfoBO> convertListInfoToBO(List<SubjectInfo> subjectInfoList);

    List<SubjectAnswerBO> convertEntityToBoList(List<SubjectJudge> subjectJudges);

    SubjectInfoBO convertEntityToBo(SubjectOptionBO subjectOptionBO,SubjectInfo subjectInfo);



}
