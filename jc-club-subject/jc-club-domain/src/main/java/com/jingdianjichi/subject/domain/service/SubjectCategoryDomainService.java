package com.jingdianjichi.subject.domain.service;

import com.jingdianjichi.subject.domain.entity.SubjectCategoryBO;

import java.util.List;

public interface SubjectCategoryDomainService {

   /*新增*/
    void add(SubjectCategoryBO subjectCategoryBO);


    List<SubjectCategoryBO> queryCategory(SubjectCategoryBO subjectCategoryBO);

    Boolean update(SubjectCategoryBO subjectCategoryBo);

    Boolean delete(SubjectCategoryBO subjectCategoryBo);
}
