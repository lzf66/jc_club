package com.jingdianjichi.subject.domain.service;

import com.jingdianjichi.subject.domain.entity.SubjectCategoryBO;
import com.jingdianjichi.subject.domain.entity.SubjectLabelBO;

import java.util.List;

public interface SubjectLabelDomainService {

   /*新增分类*/
    Boolean add(SubjectLabelBO subjectLabelBO);

    /*更新分类*/
    Boolean update(SubjectLabelBO subjectLabelBO);

    /*删除标签*/
    Boolean delete(SubjectLabelBO subjectLabelBO);

    /*查询分类下标签*/
    List<SubjectLabelBO> queryLabelByCategoryId(SubjectLabelBO subjectLabelBO);
}
