package com.jingdianjichi.subject.domain.convert;

import com.jingdianjichi.subject.domain.entity.SubjectCategoryBO;
import com.jingdianjichi.subject.infra.basic.entity.SubjectCategory;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SubjectCategoryConvert {
SubjectCategoryConvert INSTANCE = Mappers.getMapper(SubjectCategoryConvert.class);
SubjectCategory convertBoToCategory(SubjectCategoryBO subjectCategoryBO);
List<SubjectCategoryBO> convertCategoryLToBOL(List<SubjectCategory> subjectCategoryList);


}
