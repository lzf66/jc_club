package com.jingdianjichi.subject.domain.convert;

import com.jingdianjichi.subject.domain.entity.SubjectAnswerBO;
import com.jingdianjichi.subject.infra.basic.entity.SubjectMultiple;
import com.jingdianjichi.subject.infra.basic.entity.SubjectRadio;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface SubjectMultipleConvert {
SubjectMultipleConvert INSTANCE = Mappers.getMapper(SubjectMultipleConvert.class);


    SubjectMultiple convertBoToEntity(SubjectAnswerBO subjectAnswerBO);
    List<SubjectAnswerBO> convertEntityToBoList(List<SubjectMultiple> subjectMultipleList);





}
