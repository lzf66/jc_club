package com.jingdianjichi.subject.domain.entity;

import com.jingdianjichi.subject.common.entity.PageInfo;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 题目信息表(SubjectInfo)实体类
 *
 * @author makejava
 * @since 2024-06-20 21:21:50
 */
@Data
public class SubjectOptionBO implements Serializable {

    /**
     * 答案选项
     */
    private List<SubjectAnswerBO> optionList;

    /**
     * 题目答案
     */
    private String subjectAnswer;

}

