package com.jingdianjichi.subject.domain.handle.subject;

import com.jingdianjichi.subject.common.enums.SubjectTypeInfoEnum;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class SubjectTypeHandleFactory implements InitializingBean {

    @Resource
    private List<SubjectTypeHandler> subjectTypeHandlers;
    private Map<SubjectTypeInfoEnum,SubjectTypeHandler> handlerMap = new HashMap<>();

    public SubjectTypeHandler getHandler(int subjectType){
        SubjectTypeInfoEnum subjectTypeInfoEnum = SubjectTypeInfoEnum.getByCode(subjectType);
        return handlerMap.get(subjectTypeInfoEnum);
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for(SubjectTypeHandler subjectTypeHandler : subjectTypeHandlers){
            handlerMap.put(subjectTypeHandler.getHandlerType(),subjectTypeHandler);
        }
    }
}
