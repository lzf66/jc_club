package com.jingdianjichi.subject.domain.handle.subject;

import com.jingdianjichi.subject.common.enums.SubjectTypeInfoEnum;
import com.jingdianjichi.subject.domain.entity.SubjectInfoBO;
import com.jingdianjichi.subject.domain.entity.SubjectOptionBO;


public interface SubjectTypeHandler {

    /*枚举身份的识别*/
    SubjectTypeInfoEnum getHandlerType();

    /*实际题目的插入*/
    void add(SubjectInfoBO subjectInfoBO);
    SubjectOptionBO query(int subjectId);
}
