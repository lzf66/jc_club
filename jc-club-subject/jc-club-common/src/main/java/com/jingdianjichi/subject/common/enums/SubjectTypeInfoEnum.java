package com.jingdianjichi.subject.common.enums;

import lombok.Getter;

@Getter
public enum SubjectTypeInfoEnum {
    SINGLE(1,"单选"),
    MULTIPY(2,"多选"),
    JUDGE(3,"判断"),
    BRIEF(4,"多选"),
    ;

    public int code;

    public String desc;

    SubjectTypeInfoEnum(int code, String desc){
        this.code =code;
        this.desc=desc;
    }
    public static SubjectTypeInfoEnum getByCode(int codeVal){
        for (SubjectTypeInfoEnum resultCodeEnum : SubjectTypeInfoEnum.values()){
            if(resultCodeEnum.code == codeVal){
                return  resultCodeEnum;
            }
        }
        return null;
    }
}
