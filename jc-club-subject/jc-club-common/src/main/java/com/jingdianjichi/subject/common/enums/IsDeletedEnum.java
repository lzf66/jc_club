package com.jingdianjichi.subject.common.enums;

import lombok.Getter;

@Getter
public enum IsDeletedEnum {
    ISDELETED(1,"已删除"),
    UNDELETED(0,"未删除");

    public int code;

    public String desc;

    IsDeletedEnum(int code, String desc){
        this.code =code;
        this.desc=desc;
    }
    public static IsDeletedEnum getByCode(int codeVal){
        for (IsDeletedEnum resultCodeEnum : IsDeletedEnum.values()){
            if(resultCodeEnum.code == codeVal){
                return  resultCodeEnum;
            }
        }
        return null;
    }
}
